# Flan

The repository for the Flan.
Link to site: https://brothersonline.gitlab.io/flan

## Origin of project name

Flan stands for Free LAN.

## Resize an image before adding it.

1. Download an image of the game`
2. Go to `https://picresize.com/`
3. Upload your image. Set width to 300 and select .jpg as file extension.
4. Save the file. You're already done!


## Adding a game

1. Add the game to the `/src/gameData.json`
2. Make sure the install size is correct. (small: below 1GB, medium, above 1GB & below 10GB, large: above 10GB)
3. Add a banner in `/public/gameImages`
4. You're already done!
