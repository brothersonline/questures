import React, {useState} from 'react';
import getFeatureClass from "../Functions/classes";
import gamesData from "../gamesData";
import GamesList from "../Components/GamesList";

const filterResultsOnInput = (gamesList, inputFilterString) => {
  return gamesList.filter(function (item) {
    return item.name.toLowerCase().includes(inputFilterString.toLowerCase());
  });
};

const filterOnSupported = (gamesList, activeFeatureFilter) => {
  return gamesList.filter(function (item) {
    return `${activeFeatureFilter}` in item.features && item.features[`${activeFeatureFilter}`] === true;
  });
};

const filterOnUnsupported = (gamesList, activeFeatureFilter) => {
  return gamesList.filter(function (item) {
    return `${activeFeatureFilter}` in item.features && item.features[`${activeFeatureFilter}`] === false;
  });
};

const filterOnUnknown = (gamesList, activeFeatureFilter) => {
  return gamesList.filter(function (item) {
    return !(`${activeFeatureFilter}` in item.features && item.features);
  });
};

function sortListByName(filteredGameRows) {
  return filteredGameRows.sort((a, b) => (a.name > b.name) ? 1 : -1)
}

function DefaultView({setActiveView}) {
  const [inputFilter, setInputFilter] = useState("");
  const [activeFeatureFilter, setActiveFeatureFilter] = useState(gamesData.allFeatures[0]);
  const [explanationIsOpen, setExplanationIsOpen] = useState(false);

  const toggleExplanation = () => {
    setExplanationIsOpen(!explanationIsOpen);
  }

  function updateInputFilter(e){
    setInputFilter(e.target.value);
  }

  function toggleFeature(filterString){
    setActiveFeatureFilter(filterString);
  }

  let filteredGameRows = gamesData.games.slice(0);
  filteredGameRows = filterResultsOnInput(filteredGameRows, inputFilter);
  filteredGameRows = sortListByName(filteredGameRows);
  const supportedFilteredGameRows = filterOnSupported(filteredGameRows, activeFeatureFilter);
  const unsupportedFilteredGameRows = filterOnUnsupported(filteredGameRows, activeFeatureFilter);
  const unknownFilteredGameRows = filterOnUnknown(filteredGameRows, activeFeatureFilter);

  const shouldShowSupportedList = activeFeatureFilter !== "tactsuit";
  const shouldShowUnsupportedList = activeFeatureFilter !== "tactsuit";
  const shouldShowUnknownList = activeFeatureFilter !== "tactsuit";

  return (
    <>
    <header id="home">
      <section className="hero">
        <div className="container">
          <div className="row">
            <div className="col-md-8 col-md-offset-2 text-center">
              {/*<img className="animated fadeInDown hero-heading-image" src={`${process.env.PUBLIC_URL}/img/flan-logo.png`} alt="logo"/>*/}
            </div>
          </div>

      <button className="btn feature-btn help-btn" onClick={() => toggleExplanation()}><i className={`far fa-question-circle`} /></button>

      {explanationIsOpen && (
        <div className="help-dialog">
            <h3>Explanation</h3>
            <p>
              Supports feature: <i className={`far fa-check-circle`} style={{color: "green"}}/><br/>
              Doesn't support feature: <i className={`far fa-times-circle`} style={{color: "red"}}/><br/>
              Currently not known yet: <i className={`far fa-question-circle`} style={{color: "#ffc200"}}/><br/>
            </p>

            <p>
            Found games that need updating? Edit them <a href={"https://gitlab.com/-/ide/project/brothersonline/questures/tree/master/-/src/gamesData.json/"}>here!</a>
          </p>
        </div>
      )}

          <div className="row text-center animated fadeInDown">
            <div className="col-md-12 wp4">
              <input className={"hero-filter-input"} placeholder={"Search for a game"} onChange={updateInputFilter}/>
            </div>
          </div>
          <div className="row text-center animated fadeInUp hero-button-row">
            <div className="col-md-12 wp4">
              {gamesData.allFeatures.map((feature) => (
                <button
                  key={feature}
                  className={`btn feature-btn ${getFeatureClass(feature)} ${activeFeatureFilter === feature ? "" : "inactive-filter-btn"}`}
                  onClick={() => toggleFeature(feature)}>{feature}</button>
              ))}
            </div>
          </div>
        </div>
      </section>
    </header>
    <section className="games text-center section-padding">
      <h1>Supported</h1>
      {activeFeatureFilter === "tactsuit" && <p>An updated list of all tactsuit supported games can be found here: <a href="https://www.bhaptics.com/experiences/vr">Bhaptics Official Website</a>.</p>}
      {activeFeatureFilter === "family-sharing" && <p>All Quest store apps added after February 12, 2021 have Family Sharing enabled by default.</p>}
      {activeFeatureFilter === "family-sharing" && <p>App Lab games now also support Family Sharing.</p>}
      {activeFeatureFilter === "family-sharing" && <p>All free apps are listed as to have Family Sharing.</p>}
      {activeFeatureFilter === "cloud-save" && <p>All Quest store apps (and future apps) now support Cloud Save.</p>}
      {shouldShowSupportedList && (<GamesList filteredGameRows={supportedFilteredGameRows} activeFeatureFilter={activeFeatureFilter} />)}
      <h1>Unsupported</h1>
      {shouldShowUnsupportedList && (<GamesList filteredGameRows={unsupportedFilteredGameRows} activeFeatureFilter={activeFeatureFilter} />)}
      <h1>Not yet known</h1>
      {shouldShowUnknownList && (<GamesList filteredGameRows={unknownFilteredGameRows} activeFeatureFilter={activeFeatureFilter} />)}
    </section>
  </>
  );
}

export default DefaultView;
