import React, {useState} from 'react';
import DefaultView from "./Views/DefaultView";
import ReactGA from "react-ga4";

const trackingId = "G-9GQJ8VLVD5";
ReactGA.initialize(trackingId);

function App() {
  ReactGA.pageview(window.location.pathname + window.location.search);

  const [activeView, setActiveView] = useState(0);

  return (
    <div className="App">
      {activeView === 0 && (<DefaultView setActiveView={setActiveView} />)}
    </div>
  );
}

export default App;
