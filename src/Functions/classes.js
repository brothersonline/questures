const featureClasses = {
    "family-sharing": "family-sharing-btn",
    "cross-buy": "cross-buy-btn",
    "90hz": "90hz-btn",
    "120hz": "120hz-btn",
    "cloud-save": "cloud-save-btn",
};

const getFeatureClass = (keyName) => {
  try {
    return featureClasses[keyName];
  } catch {
    return '';
  }
};

export default getFeatureClass;
