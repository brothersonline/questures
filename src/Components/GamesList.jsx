import React from 'react';
import "./GamesList.css";

function GamesList({filteredGameRows = [], activeFeatureFilter}) {
  function getIconClass(game) {
    if (!(`${activeFeatureFilter}` in game.features)) {
      return 'fa-question-circle';
    }
    return game.features[activeFeatureFilter] ? 'fa-check-circle' : 'fa-times-circle';
  }

  function getIconColor(game) {
    if (!(`${activeFeatureFilter}` in game.features)) {
      return "#ffc200";
    }
    return game.features[activeFeatureFilter] ? "green" : "red";
  }

  return (
    <div className="container">
      {filteredGameRows.length === 0 &&
      <div>No games where found</div>
      }
      <div className="row text-center animated fadeInUp">
        <div className="col-md-12 wp4 games-list">
          {filteredGameRows.map((game, i) => (
              <div className={"class-row"} key={game.name}>
                <div className={"class-row-character-name"}>
                  <h1><i className={`far ${getIconClass(game)}`} style={{color: getIconColor(game)}}/> {game.name}</h1>
                </div>
              </div>
            ))}
        </div>
      </div>
    </div>
  );
}

export default GamesList;
